<?php 
Class User_appointment extends CI_Controller{
	function index(){
		
		$this->load->view('medicare/index');

	}
	function add_appointment(){
	if(isset($_POST) && count($_POST) > 0)     
        { 
            $params = array(
				'clinic_name' => $this->input->post('clinic_name'),
				'service_name' => $this->input->post('service_name'),
				'patient_name' => $this->input->post('patient_name'),
				'phone_number' => $this->input->post('phone_number'),
				'email' => $this->input->post('email'),
				'date' => $this->input->post('date'),
				'time' => $this->input->post('time')	
            );
            $this->load->model('admin_model/Appointment_model');
            $appointment_id = $this->Appointment_model->add_appointment($params);
            redirect('User_appointment/successful');
        }
        else
        {            
            $data['_view'] = 'medicare/additional-service';
            $this->load->view('medicare/additional-service',$data);
        }
}
function successful(){
	echo "<script>alert('Your appointment is scheduled. Thank you!');
	window.location.href='index';
	
	</script>";
	

}
}
?>