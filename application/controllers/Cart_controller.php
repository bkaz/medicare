<?php
Class Cart_controller extends CI_Controller{
	function Cart(){
		parent::controller();
		$this->load->model('Cart_model'); 
	}
	function index(){
		$this->load->model('Cart_model');
		$data['product']=$this->Cart_model->retrieve_products();
		$this->load->view('medicare/shop-detail',$data);
	}
}
?>