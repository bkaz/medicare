<?php

class Service extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model/Service_model');
    } 

    /*
     * Listing of service
     */
    function index()
    {
        $data['service'] = $this->Service_model->get_all_service();
        
        $data['_view'] = 'admin_view/service/index';
        $this->load->view('admin_view/layouts/main',$data);
    }

    /*
     * Adding a new service
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'service_name' => $this->input->post('service_name'),
				'service_type' => $this->input->post('service_type'),
				'description' => $this->input->post('description'),
            );
            
            $service_id = $this->Service_model->add_service($params);
            redirect('service/index');
        }
        else
        {            
            $data['_view'] = 'admin_view/service/add';
            $this->load->view('admin_view/layouts/main',$data);
        }
    }  

    /*
     * Editing a service
     */
    function edit($service_id)
    {   
        // check if the service exists before trying to edit it
        $data['service'] = $this->Service_model->get_service($service_id);
        
        if(isset($data['service']['service_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'service_name' => $this->input->post('service_name'),
					'service_type' => $this->input->post('service_type'),
					'description' => $this->input->post('description'),
                );

                $this->Service_model->update_service($service_id,$params);            
                redirect('service/index');
            }
            else
            {
                $data['_view'] = 'admin_view/service/edit';
                $this->load->view('admin_view/layouts/main',$data);
            }
        }
        else
            show_error('The service you are trying to edit does not exist.');
    } 

    /*
     * Deleting service
     */
    function remove($service_id)
    {
        $service = $this->Service_model->get_service($service_id);

        // check if the service exists before trying to delete it
        if(isset($service['service_id']))
        {
            $this->Service_model->delete_service($service_id);
            redirect('service/index');
        }
        else
            show_error('The service you are trying to delete does not exist.');
    }
    
}
