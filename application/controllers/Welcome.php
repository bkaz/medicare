<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
	$this->load->view('medicare/index.php');
		
	}

	/*Function starts for user registration*/
	public function signup()
	{
	$this->load->helper('form');
	$this->load->view('medicare/signup.php');
		
	}
	public function signup1()
	{
	$data_form= $this->input->POST(Null, TRUE);
	if ($data_form) {
			$first_name = $data_form['first_name'];
			$last_name = $data_form['last_name'];
			$email = $data_form['email'];
			$password = $data_form['password'];
			$confirm_password = $data_form['password_confirmation'];
			$datas =  array(
				'user_id' =>NULL , 
				'first_name' =>$first_name , 
				'last_name' =>$last_name , 
				'email' =>$email ,
				'password' =>md5($password) ,
				'confirm_password' =>md5($confirm_password)
			);
			$this->load->model('Signup_model');
			$this->Signup_model->index($datas);	
			redirect('Welcome/index');
			}
			$this->load->view('signup');
	}
	/*Function ends for user registration*/
	
	public function service(){
		$this->load->view('medicare/service.php');
	}
	public function additional_service(){
		$this->load->view('medicare/additional-service.php');
	}
	public function cart(){
		$this->load->view('medicare/cart.php');
	}
	public function contact(){
		$this->load->view('medicare/contact.php');
	}
	public function cosmetic_solutions(){
		$this->load->view('medicare/cosmetic-solutions-service.php');
	}
	public function dentures(){
		$this->load->view('medicare/dentures-service.php');
	}
	public function diagonsis(){
		$this->load->view('medicare/diagnostic-service.php');
	}
	public function doctor(){
		$this->load->view('medicare/doctor.php');
	}
	public function general_service(){
		$this->load->view('medicare/general-service.php');
	}
	public function orthodontics_service(){
		$this->load->view('medicare/orthodontics-service.php');
	}
	public function pediatric_service(){
		$this->load->view('medicare/pediatric-service.php');
	}
	public function restorative_service(){
		$this->load->view('medicare/restorative-service.php');
	}
	public function shop_detail(){
		$this->load->view('medicare/shop-detail.php');
	}
	public function shop(){
		$this->load->view('medicare/shop.php');
	}
	public function timetabe(){
		$this->load->view('medicare/timetable.php');
	}



}

