<?php

class Admin_login extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_login_model');
    } 

    /*
     * Listing of admin_login
     */
    function index()
    {
        $data['admin_login'] = $this->Admin_login_model->get_all_admin_login();
        
        $data['_view'] = 'admin_login/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new admin_login
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
            );
            
            $admin_login_id = $this->Admin_login_model->add_admin_login($params);
            redirect('admin_login/index');
        }
        else
        {            
            $data['_view'] = 'admin_login/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a admin_login
     */
    function edit($admin_id)
    {   
        // check if the admin_login exists before trying to edit it
        $data['admin_login'] = $this->Admin_login_model->get_admin_login($admin_id);
        
        if(isset($data['admin_login']['admin_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email'),
                );

                $this->Admin_login_model->update_admin_login($admin_id,$params);            
                redirect('admin_login/index');
            }
            else
            {
                $data['_view'] = 'admin_login/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The admin_login you are trying to edit does not exist.');
    } 

    /*
     * Deleting admin_login
     */
    function remove($admin_id)
    {
        $admin_login = $this->Admin_login_model->get_admin_login($admin_id);

        // check if the admin_login exists before trying to delete it
        if(isset($admin_login['admin_id']))
        {
            $this->Admin_login_model->delete_admin_login($admin_id);
            redirect('admin_login/index');
        }
        else
            show_error('The admin_login you are trying to delete does not exist.');
    }
    
}
