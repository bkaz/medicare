<?php

class Clinic_staff extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model/Clinic_staff_model');
    } 

    /*
     * Listing of clinic_staff
     */
    function index()
    {
        $data['clinic_staff'] = $this->Clinic_staff_model->get_all_clinic_staff();
        
        $data['_view'] = 'admin_view/clinic_staff/index';
        $this->load->view('admin_view/layouts/main',$data);
    }

    /*
     * Adding a new clinic_staff
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
          
            $params = array(
				'staff_name' => $this->input->post('staff_name'),
				'staff_post' => $this->input->post('staff_post'),
				'staff_description' => $this->input->post('staff_description'),
				'staff_status' => $this->input->post('staff_status'),
            );
            $this->load->model('admin_model/Clinic_staff_model');
            $clinic_staff_id = $this->Clinic_staff_model->add_clinic_staff($params);
            redirect('admin_panel/clinic_staff/index');
        }
        else
        {            
            $data['_view'] = 'admin_view/clinic_staff/add';
            $this->load->view('admin_view/layouts/main',$data);
        }
    }  

    /*
     * Editing a clinic_staff
     */
    function edit($staff_id)
    {   
        // check if the clinic_staff exists before trying to edit it
        $data['clinic_staff'] = $this->Clinic_staff_model->get_clinic_staff($staff_id);
        
        if(isset($data['clinic_staff']['staff_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'staff_name' => $this->input->post('staff_name'),
					'staff_post' => $this->input->post('staff_post'),
					'staff_description' => $this->input->post('staff_description'),
					'staff_status' => $this->input->post('staff_status'),
                );

                $this->Clinic_staff_model->update_clinic_staff($staff_id,$params);            
                redirect('admin_panel/clinic_staff/index');
            }
            else
            {
                $data['_view'] = 'admin_view/clinic_staff/edit';
                $this->load->view('admin_view/layouts/main',$data);
            }
        }
        else
            show_error('The clinic_staff you are trying to edit does not exist.');
    } 

    /*
     * Deleting clinic_staff
     */
    function remove($staff_id)
    {
        $clinic_staff = $this->Clinic_staff_model->get_clinic_staff($staff_id);

        // check if the clinic_staff exists before trying to delete it
        if(isset($clinic_staff['staff_id']))
        {
            $this->Clinic_staff_model->delete_clinic_staff($staff_id);
            redirect('admin_panel/clinic_staff/index');
        }
        else
            show_error('The clinic_staff you are trying to delete does not exist.');
    }
    
}
