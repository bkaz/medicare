<?php

class Cliniic extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model/Cliniic_model');
    } 

    /*
     * Listing of cliniic
     */
    function index()
    {
        $data['cliniic'] = $this->Cliniic_model->get_all_cliniic();
        
        $data['_view'] = 'admin_view/cliniic/index';
        $this->load->view('admin_view/layouts/main',$data);
    }

    /*
     * Adding a new cliniic
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'clinic_name' => $this->input->post('clinic_name'),
            );
            
            $cliniic_id = $this->Cliniic_model->add_cliniic($params);
            redirect('admin_panel/cliniic/index');
        }
        else
        {            
            $data['_view'] = 'admin_view/cliniic/add';
            $this->load->view('admin_view/layouts/main',$data);
        }
    }  

    /*
     * Editing a cliniic
     */
    function edit($clinic_id)
    {   
        // check if the cliniic exists before trying to edit it
        $data['cliniic'] = $this->Cliniic_model->get_cliniic($clinic_id);
        
        if(isset($data['cliniic']['clinic_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'clinic_name' => $this->input->post('clinic_name'),
                );

                $this->Cliniic_model->update_cliniic($clinic_id,$params);            
                redirect('admin_panel/cliniic/index');
            }
            else
            {
                $data['_view'] = 'admin_view/cliniic/edit';
                $this->load->view('admin_view/layouts/main',$data);
            }
        }
        else
            show_error('The cliniic you are trying to edit does not exist.');
    } 

    /*
     * Deleting cliniic
     */
    function remove($clinic_id)
    {
        $cliniic = $this->Cliniic_model->get_cliniic($clinic_id);

        // check if the cliniic exists before trying to delete it
        if(isset($cliniic['clinic_id']))
        {
            $this->Cliniic_model->delete_cliniic($clinic_id);
            redirect('admin_panel/cliniic/index');
        }
        else
            show_error('The cliniic you are trying to delete does not exist.');
    }
    
}
