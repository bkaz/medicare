<?php
class Appointment extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model/Appointment_model');
    } 

    /*
     * Listing of appointment
     */
    function index()
    {
        $this->load->model('admin_model/Appointment_model');
       
        $data['appointment'] = $this->Appointment_model->get_all_appointment();

        $data['_view'] = 'admin_view/appointment/index';
        $this->load->view('admin_view/layouts/main',$data);
    }

    /*
     * Adding a new appointment
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'clinic_name' => $this->input->post('clinic_name'),
				'service_name' => $this->input->post('service_name'),
				'patient_name' => $this->input->post('patient_name'),
				'phone_number' => $this->input->post('phone_number'),
				'email' => $this->input->post('email'),
				'date' => $this->input->post('date'),
				'time' => $this->input->post('time'),
				'doctor_id' => $this->input->post('doctor_id'),
            );
            $this->load->model('admin_model/Appointment_model');
            $appointment_id = $this->Appointment_model->add_appointment($params);
            redirect('admin_panel/appointment/index');
        }
        else
        {            
            $data['_view'] = 'admin_view/appointment/add';
            $this->load->view('admin_view/layouts/main',$data);
        }
    }  

    /*
     * Editing a appointment
     */
    function edit($appointment_id)
    {   
        // check if the appointment exists before trying to edit it
        $data['appointment'] = $this->Appointment_model->get_appointment($appointment_id);
        
        if(isset($data['appointment']['appointment_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'clinic_name' => $this->input->post('clinic_name'),
					'service_name' => $this->input->post('service_name'),
					'patient_name' => $this->input->post('patient_name'),
					'phone_number' => $this->input->post('phone_number'),
					'email' => $this->input->post('email'),
					'date' => $this->input->post('date'),
					'time' => $this->input->post('time'),
					'doctor_id' => $this->input->post('doctor_id'),
                );

                $this->Appointment_model->update_appointment($appointment_id,$params);            
                redirect('admin_panel/appointment/index');
            }
            else
            {
                $data['_view'] = 'admin_view/appointment/edit';
                $this->load->view('admin_view/layouts/main',$data);
            }
        }
        else
            show_error('The appointment you are trying to edit does not exist.');
    } 

    /*
     * Deleting appointment
     */
    function remove($appointment_id)
    {
        $appointment = $this->Appointment_model->get_appointment($appointment_id);

        // check if the appointment exists before trying to delete it
        if(isset($appointment['appointment_id']))
        {
            $this->Appointment_model->delete_appointment($appointment_id);
            redirect('admin_panel/appointment/index');
        }
        else
            show_error('The appointment you are trying to delete does not exist.');
    }
    
}
