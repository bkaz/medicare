<?php

class User_register extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_register_model');
    } 

    /*
     * Listing of user_register
     */
    function index()
    {
        $data['user_register'] = $this->User_register_model->get_all_user_register();
        
        $data['_view'] = 'user_register/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new user_register
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'password' => $this->input->post('password'),
				'confirm_password' => $this->input->post('confirm_password'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
            );
            
            $user_register_id = $this->User_register_model->add_user_register($params);
            redirect('user_register/index');
        }
        else
        {            
            $data['_view'] = 'user_register/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a user_register
     */
    function edit($user_id)
    {   
        // check if the user_register exists before trying to edit it
        $data['user_register'] = $this->User_register_model->get_user_register($user_id);
        
        if(isset($data['user_register']['user_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'password' => $this->input->post('password'),
					'confirm_password' => $this->input->post('confirm_password'),
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
                );

                $this->User_register_model->update_user_register($user_id,$params);            
                redirect('user_register/index');
            }
            else
            {
                $data['_view'] = 'user_register/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The user_register you are trying to edit does not exist.');
    } 

    /*
     * Deleting user_register
     */
    function remove($user_id)
    {
        $user_register = $this->User_register_model->get_user_register($user_id);

        // check if the user_register exists before trying to delete it
        if(isset($user_register['user_id']))
        {
            $this->User_register_model->delete_user_register($user_id);
            redirect('user_register/index');
        }
        else
            show_error('The user_register you are trying to delete does not exist.');
    }
    
}
