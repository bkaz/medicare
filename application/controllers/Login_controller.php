<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Login_controller extends CI_Controller{
	function index(){
		$data_form= $this->input->POST(Null, TRUE);
		if ($data_form) {
		$username=$data_form['username'];
		$password=$data_form['password'];
		$dataArray = array(
			'username' =>$username,
			'password'=> $password 
		);
		$this->load->model('Login_model');
		$this->Login_model->index($dataArray);
		redirect('Welcome/cart');
		}
		$this->load->view('index');
	}
}
?>