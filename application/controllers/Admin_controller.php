<?php
Class Admin_controller extends CI_Controller{
	function index(){

		$this->load->helper('form');
		$this->load->view('medicare/admin_login.php');
	}
	function admin_login(){
		$data_form= $this->input->POST(Null, TRUE);
		if ($data_form) {
			$username=$data_form['username'];
			$password=$data_form['password'];
			$adminArray = array(
				'username' =>$username ,
				'password'=>md5($password)
				 );
			$this->load->model('Admin_login_model');
			$this->Admin_login_model->index($adminArray);
			redirect('Admin_controller/admin_panel');
		}
		$this->load->view('index');
	}
	function admin_panel(){
		$this->load->view('admin_view/layouts/main');
	}
}

?>