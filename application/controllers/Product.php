<?php

class Product extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model/Product_model');
    } 

    /*
     * Listing of product
     */
    function index()
    {
        $data['product'] = $this->Product_model->get_all_product();

        $data['_view'] = 'admin_view/product/index';
        $this->load->view('admin_view/layouts/main',$data);
    }

    /*
     * Adding a new product
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'product_name' => $this->input->post('product_name'),
				'product_description' => $this->input->post('product_description'),
				'price' => $this->input->post('price'),
            );
            
            $product_id = $this->Product_model->add_product($params);
            redirect('product/index');
        }
        else
        {            
            $data['_view'] = 'admin_view/product/add';
            $this->load->view('admin_view/layouts/main',$data);
        }
    }  

    /*
     * Editing a product
     */
    function edit($product_id)
    {   
        // check if the product exists before trying to edit it
        $data['product'] = $this->Product_model->get_product($product_id);
       
        if(isset($data['product']['product_id']))
        {

            if(isset($_POST) && count($_POST) > 0)     
            {  

                $params = array(
					'product_name' => $this->input->post('product_name'),
					'product_description' => $this->input->post('product_description'),
					'price' => $this->input->post('price'),
                );

                $this->Product_model->update_product($product_id,$params);

                redirect('product/index');
            }
            else
            {
                $data['_view'] = 'admin_view/product/edit';
                $this->load->view('admin_view/layouts/main',$data);
            }
        }
        else
            show_error('The product you are trying to edit does not exist.');
    } 

    /*
     * Deleting product
     */
    function remove($product_id)
    {

        $product = $this->Product_model->get_product($product_id);

        // check if the product exists before trying to delete it
        if(isset($product['product_id']))
        { 
            $this->Product_model->delete_product($product_id);
            redirect('product/index');
        }
        else
            show_error('The product you are trying to delete does not exist.');
    }
    
}
