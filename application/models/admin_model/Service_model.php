<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Service_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get service by service_id
     */
    function get_service($service_id)
    {
        return $this->db->get_where('service',array('service_id'=>$service_id))->row_array();
    }
        
    /*
     * Get all service
     */
    function get_all_service()
    {
        $this->db->order_by('service_id', 'desc');
        return $this->db->get('service')->result_array();
    }
        
    /*
     * function to add new service
     */
    function add_service($params)
    {
        $this->db->insert('service',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update service
     */
    function update_service($service_id,$params)
    {
        $this->db->where('service_id',$service_id);
        return $this->db->update('service',$params);
    }
    
    /*
     * function to delete service
     */
    function delete_service($service_id)
    {
        return $this->db->delete('service',array('service_id'=>$service_id));
    }
}
