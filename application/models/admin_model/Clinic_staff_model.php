<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinic_staff_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get clinic_staff by staff_id
     */
    function get_clinic_staff($staff_id)
    {
        return $this->db->get_where('clinic_staff',array('staff_id'=>$staff_id))->row_array();
    }
        
    /*
     * Get all clinic_staff
     */
    function get_all_clinic_staff()
    {
        $this->db->order_by('staff_id', 'desc');
        return $this->db->get('clinic_staff')->result_array();
    }
        
    /*
     * function to add new clinic_staff
     */
    function add_clinic_staff($params)
    {
      
        $this->db->insert('clinic_staff',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update clinic_staff
     */
    function update_clinic_staff($staff_id,$params)
    {
        $this->db->where('staff_id',$staff_id);
        return $this->db->update('clinic_staff',$params);
    }
    
    /*
     * function to delete clinic_staff
     */
    function delete_clinic_staff($staff_id)
    {
        return $this->db->delete('clinic_staff',array('staff_id'=>$staff_id));
    }
}
