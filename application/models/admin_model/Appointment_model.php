<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get appointment by appointment_id
     */
    function get_appointment($appointment_id)
    {
        return $this->db->get_where('appointment',array('appointment_id'=>$appointment_id))->row_array();
    }
        
    /*
     * Get all appointment
     */
    function get_all_appointment()
    {
        $this->db->order_by('appointment_id', 'desc');
        return $this->db->get('appointment')->result_array();
    }
        
    /*
     * function to add new appointment
     */
    function add_appointment($params)
    {
        $this->db->insert('appointment',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update appointment
     */
    function update_appointment($appointment_id,$params)
    {
        $this->db->where('appointment_id',$appointment_id);
        return $this->db->update('appointment',$params);
    }
    
    /*
     * function to delete appointment
     */
    function delete_appointment($appointment_id)
    {
        return $this->db->delete('appointment',array('appointment_id'=>$appointment_id));
    }
}
