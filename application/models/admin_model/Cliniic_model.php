<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cliniic_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get cliniic by clinic_id
     */
    function get_cliniic($clinic_id)
    {
        return $this->db->get_where('cliniic',array('clinic_id'=>$clinic_id))->row_array();
    }
        
    /*
     * Get all cliniic
     */
    function get_all_cliniic()
    {
        $this->db->order_by('clinic_id', 'desc');
        return $this->db->get('cliniic')->result_array();
    }
        
    /*
     * function to add new cliniic
     */
    function add_cliniic($params)
    {
        $this->db->insert('cliniic',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update cliniic
     */
    function update_cliniic($clinic_id,$params)
    {
        $this->db->where('clinic_id',$clinic_id);
        return $this->db->update('cliniic',$params);
    }
    
    /*
     * function to delete cliniic
     */
    function delete_cliniic($clinic_id)
    {
        return $this->db->delete('cliniic',array('clinic_id'=>$clinic_id));
    }
}
