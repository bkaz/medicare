<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php include "includes/header.php";?>

	<!-- Content Start -->
	<section class="content inner-pg service-pg clearfix">
		<!-- Breadcrumb Start -->
		<div class="breadcrumb-title clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-left">
							<ol class="breadcrumb">
								<li>
									<a href="index.php">HOME</a>
								</li>
								<li class="active">SERVICES</li>
							</ol><!--/.breadcrumb-->
						</div><!--/.breadcrumb-left-->
					</div><!--/.col-sm-6 col-md-6-->
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-right">
							<h5>Services</h5>
						</div>
					</div><!--/.col-sm-6 col-md-6-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!-- Breadcrumb End -->
		<div class="container">
			<!-- Inner Pages Start -->
			<div class="inner-content clearfix">
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-9">
						<!-- Content Description Start -->
						<div class="content-desc clearfix">
							<!-- Banner Start -->
							<div class="banner clearfix">
								<img alt="BANNER" class="img-responsive" src="<?= base_url('assets/images/medibg.jpg')?>">
								<div class="banner-overlay"></div><!-- Banner Detail Start -->
								<div class="banner-desc clearfix">
									<h4><span>Providing Total</span> Health Care Solution</h4><a class="btn btn-default" href="#" role="button">Make an Appointment</a>
								</div><!-- Banner Detail End -->
							</div><!-- Banner End -->
							<!-- Service Detail Block Start -->
							<div class="service-detail-block white-bg clearfix">
								<h4>Surgery</h4>
								<p>Phasellus velit risus, euismod a lacus et, mattis condimentum augue. Vivamus fermentum ex quis imperdiet sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida euismod a lacus et, mattis condimentum augue.</p>
							</div><!-- Service Detail Block End -->
							<!-- Human Body Parts Start -->
							<div class="human-body-parts clearfix">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon purple-txt">
												<i class="flaticon-human-lungs"></i>
											</div>
											<h6>Pulmonary</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon red-txt">
												<i class="flaticon-human-heart"></i>
											</div>
											<h6>Cardiology</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon red-txt">
												<i class="flaticon-human-head"></i>
											</div>
											<h6>Cosmetic Solutions</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon green-txt">
												<i class="flaticon-eyeball-structure"></i>
											</div>
											<h6>Eye</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon sky-txt">
												<i class="flaticon-tooth-and-gums"></i>
											</div>
											<h6>Dental care</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon orange-txt">
												<i class="flaticon-human-ear"></i>
											</div>
											<h6>Ear treatment</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon dark-green-txt">
												<i class="flaticon-basophil"></i>
											</div>
											<h6>Urology</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon maroon-txt">
												<i class="flaticon-spine-bone"></i>
											</div>
											<h6>X-ray</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon violet-txt">
												<i class="flaticon-neuron"></i>
											</div>
											<h6>Neurology</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon violet-txt">
												<i class="flaticon-fertilization"></i>
											</div>
											<h6>Fertility</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon violet-txt">
												<i class="flaticon-small-intestine"></i>
											</div>
											<h6>Gastroenterology</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
								</div><!--/.row-->
							</div><!-- Human Body Parts End -->
							<!-- Appointment Form Start -->
							<?php include "appointment_form.php";?>	
							<!-- Appointment Form End -->
						</div><!-- Content Description End -->
					</div><!--/.col-sm-12 col-md-8 col-lg-9-->
					<div class="col-sm-12 col-md-4 col-lg-3">
						<!-- Sidebar Widget Start -->
						<div class="sidebar-widget clearfix">
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- Medlife Service Start -->
								<div class="medlife-service blue-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Medlife Services</h6>
									</div><!-- Widget Title End -->
									<ul>
										<li>
											<a href="general-service.php">General and Preventive Care</a>
										</li>
										<li>
											<a href="cosmetic-solutions-service.php">Cosmetic Solutions</a>
										</li>
										<li>
											<a href="restorative-service.php">Restorative Solutions</a>
										</li>
										<li>
											<a href="additional-service.php">Additional Treatments</a>
										</li>
										<li>
											<a href="orthodontics-service.php">Orthodontics</a>
										</li>
										<li>
											<a href="dentures-service.php">Dentures & Denture Repair</a>
										</li>
										<li>
											<a href="diagnostic-service.php">Diagnostic & Preventive</a>
										</li>
										<li>
											<a href="pediatric-service.php">Pediatric Dentistry</a>
										</li>
									</ul>
								</div><!-- Medlife Service End -->
								<!-- Opening Hover Start -->
								<div class="opening-hours light-green-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Opening Hours</h6>
									</div><!-- Widget Title End -->
									<table class="table">
										<tbody>
											<tr>
												<td>Monday - Friday:</td>
												<td>8.30 - 18.30</td>
											</tr>
											<tr>
												<td>Saturday:</td>
												<td>10.30 - 16.30</td>
											</tr>
											<tr>
												<td>Sunday:</td>
												<td>10.30 - 16:30</td>
											</tr>
										</tbody>
									</table>
								</div><!-- Opening Hover End -->
							</div><!-- Widget Block End -->
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- News Letter Start -->
								<div class="news-letter gray-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>News Letters</h6>
									</div><!-- Widget Title End -->
									<form action="http://your_username.dataserver.list-manage.com/subscribe/post?u=YOUR_API_KEY&amp;id=LIST_ID" class="md__newsletter-form" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
										<div>
											<input class="email form-control" name="EMAIL" placeholder="Email address" required="" type="email" value="">
											<div aria-hidden="true" style="position: absolute; left: -5000px;">
												<input name="b_78185f3823fef6dc6a261e0df_2ebd195299" tabindex="-1" type="text" value="">
											</div><button class="btn btn-default" id="mc-embedded-subscribe1" name="SEND" type="submit">Subscribe</button>
										</div>
										<div class="md__newsletter-message"></div>
									</form>
								</div><!-- News Letter End -->
							</div><!-- Widget Block End -->
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- Recent News Start -->
								<div class="recent-news white-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Recent Newss</h6>
									</div><!-- Widget Title End -->
									<a href="#">VIEW ALL</a>
									<div class="recent-block">
										<a href="#">Ultrasmall nanoparticles kill cancer cells in unusual way</a>
										<p><i aria-hidden="true" class="fa fa-clock-o"></i> September 30, 2016</p>
									</div>
									<div class="recent-block">
										<a href="#">Eye lens regeneration from own stem cells</a>
										<p><i aria-hidden="true" class="fa fa-clock-o"></i> September 30, 2016</p>
									</div>
								</div><!-- Recent News End -->
							</div><!-- Widget Block End -->
						</div><!-- Sidebar Widget End -->
					</div><!--/.col-sm-12 col-md-4 col-lg-3-->
				</div><!--/.row-->
			</div><!-- Inner Pages End -->
		</div><!--/.container-->
	</section><!-- Content End -->
	<?php include "includes/footer.php";?>

	<!-- Back To Top Start -->
	<div class="back-to-top clearfix">
		<a href="#"><span><i aria-hidden="true" class="fa fa-chevron-up"></i> Top</span></a>
	</div><!-- Back To Top End -->
