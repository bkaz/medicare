<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php include "includes/header.php";?>
	<!-- Content Start -->
	<section class="content inner-pg service-pg clearfix">
		<!-- Breadcrumb Start -->
		<div class="breadcrumb-title clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-left">
							<ol class="breadcrumb">
								<li>
									<a href="index.php">HOME</a>
								</li>
								<li class="active">Dentures & Denture Repair</li>
							</ol><!--/.breadcrumb-->
						</div><!--/.breadcrumb-left-->
					</div><!--/.col-sm-6 col-md-6-->
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-right">
							<h5>Dentures & Denture Repair</h5>
						</div>
					</div><!--/.col-sm-6 col-md-6-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!-- Breadcrumb End -->
		<div class="container">
			<!-- Inner Pages Start -->
			<div class="inner-content clearfix">
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-9">
						<!-- Content Description Start -->
						<div class="content-desc clearfix">
							<!-- Banner Start -->
							<div class="banner clearfix">
								<img alt="BANNER" class="img-responsive" src="<?= base_url('assets/images/dentures-banner.jpg');?>">
								<div class="banner-overlay"></div><!-- Banner Detail Start -->
								<div class="banner-desc txt-right clearfix">
									<h4><span>Orthodontics &</span> Dentofacial Orthopedics</h4><a class="btn btn-default" href="#" role="button">Make an Appointment</a>
								</div><!-- Banner Detail End -->
							</div><!-- Banner End -->
							<!-- Service Detail Block Start -->
							<div class="service-detail-block white-bg clearfix">
								<h4>Dentistry</h4>
								<p>Orthodontics and dentofacial orthopedics is the specialty of dentistry that focuses on the alignment of the teeth and the dental arches: the maxilla and the mandible. The practice of this specialty includes diagnosis, prevention, interception, and correction of malocclusion and other abnormalities of the developing or mature orofacial</p>
							</div><!-- Service Detail Block End -->
							<!-- Human Body Parts Start -->
							<div class="human-body-parts clearfix">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon red-txt">
												<i class="flaticon-mouth-open"></i>
											</div>
											<h6>Orthodontics</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon sky-txt">
												<i class="flaticon-tooth-and-gums"></i>
											</div>
											<h6>Dental implants</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
										<div class="body-parts-block">
											<div class="icon maroon-txt">
												<i class="flaticon-human-teeth"></i>
											</div>
											<h6>Dental x-rays</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</div><!--/.body-parts-block-->
									</div><!--/.col-xs-6 col-sm-6 col-md-6 col-lg-4-->
								</div><!--/.row-->
							</div><!-- Human Body Parts End -->
							<!-- Appointment Form Start -->
							<?php include "appointment_form.php";?>	
							<!-- Appointment Form End -->
						</div><!-- Content Description End -->
					</div><!--/.col-sm-12 col-md-8 col-lg-9-->
					<div class="col-sm-12 col-md-4 col-lg-3">
						<!-- Sidebar Widget Start -->
						<div class="sidebar-widget clearfix">
							<!-- Widget Block Start -->
							<div class="widget-block">
									<!-- Medlife Service Start -->
								<div class="medlife-service blue-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Medlife Services</h6>
									</div><!-- Widget Title End -->
									<ul>
										<li>
											<a class="active" href="<?= base_url('index.php/Welcome/general_service');?>">General and Preventive Care</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/cosmetic_solutions');?>">Cosmetic Solutions</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/restorative_service');?>">Restorative Solutions</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/additional_service');?>">Additional Treatments</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/orthodontics_service');?>">Orthodontics</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/dentures');?>">Dentures & Denture Repair</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/diagonsis');?>">Diagnostic & Preventive</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/pediatric_service');?>">Pediatric Dentistry</a>
										</li>
									</ul>
								</div><!-- Medlife Service End -->
								<!-- Opening Hover Start -->
								<div class="opening-hours light-green-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Opening Hours</h6>
									</div><!-- Widget Title End -->
									<table class="table">
										<tbody>
											<tr>
												<td>Monday - Friday:</td>
												<td>8.30 - 18.30</td>
											</tr>
											<tr>
												<td>Saturday:</td>
												<td>10.30 - 16.30</td>
											</tr>
											<tr>
												<td>Sunday:</td>
												<td>10.30 - 16:30</td>
											</tr>
										</tbody>
									</table>
								</div><!-- Opening Hover End -->
							</div><!-- Widget Block End -->
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- News Letter Start -->
								<div class="news-letter gray-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>News Letters</h6>
									</div><!-- Widget Title End -->
									<form action="http://your_username.dataserver.list-manage.com/subscribe/post?u=YOUR_API_KEY&amp;id=LIST_ID" class="md__newsletter-form" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
										<div>
											<input class="email form-control" name="EMAIL" placeholder="Email address" required="" type="email" value="">
											<div aria-hidden="true" style="position: absolute; left: -5000px;">
												<input name="b_78185f3823fef6dc6a261e0df_2ebd195299" tabindex="-1" type="text" value="">
											</div><button class="btn btn-default" id="mc-embedded-subscribe1" name="SEND" type="submit">Subscribe</button>
										</div>
										<div class="md__newsletter-message"></div>
									</form>
								</div><!-- News Letter End -->
							</div><!-- Widget Block End -->
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- Recent News Start -->
								<div class="recent-news white-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Recent Newss</h6>
									</div><!-- Widget Title End -->
									<a href="#">VIEW ALL</a>
									<div class="recent-block">
										<a href="#">Ultrasmall nanoparticles kill cancer cells in unusual way</a>
										<p><i aria-hidden="true" class="fa fa-clock-o"></i> September 30, 2016</p>
									</div>
									<div class="recent-block">
										<a href="#">Eye lens regeneration from own stem cells</a>
										<p><i aria-hidden="true" class="fa fa-clock-o"></i> September 30, 2016</p>
									</div>
								</div><!-- Recent News End -->
							</div><!-- Widget Block End -->
						</div><!-- Sidebar Widget End -->
					</div><!--/.col-sm-12 col-md-4 col-lg-3-->
				</div><!--/.row-->
			</div><!-- Inner Pages End -->
		</div><!--/.container-->
	</section><!-- Content End -->
	<?php include "includes/footer.php"?>

	<!-- Back To Top Start -->
	<div class="back-to-top clearfix">
		<a href="#"><span><i aria-hidden="true" class="fa fa-chevron-up"></i> Top</span></a>
	</div><!-- Back To Top End -->
