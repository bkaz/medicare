	<!-- Footer Start -->
	<footer class="footer clearfix">
		<div class="container">
			<!-- Footer Top Start -->
			<div class="f-top clearfix">
				<!-- F-Widget Start -->
				<div class="f-widget clearfix">
					<div class="row">
						<div class="col-sm-6 col-md-3 widget-block">
							<h6>MEDLIFE</h6><!-- Medlife Start -->
							<div class="medlife-block">
								<p>Consulting WP - Before we talk destination, we shine a spotlight across your organization to fully understand its people, processes, and technology.</p>
							</div><!-- Medlife End -->
						</div><!--/.col-sm-6 col-md-3-->
						<div class="col-sm-6 col-md-3 widget-block">
							<h6>PAGES</h6><!-- Medlife Start -->
							<div class="pages-block clearfix">
								<div class="col-xs-6 col-sm-6 col-md-6 padding">
									<ul>
										<li>
											<a href="<?= base_url('index.php/Welcome');?>">Home</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/service');?>">Services</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/timetable');?>">Appointment</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/doctor');?>">About</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/contact');?>">Contact</a>
										</li>
									</ul>
								</div><!--/.col-xs-6 col-sm-6-->
								<div class="col-xs-6 col-sm-6 col-md-6 padding">
									<ul>
										<li>
											<a href="<?= base_url('index.php/Welcome/pediatric_service');?>">Pediatry</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/orthodontics_service');?>">Orthodontics</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/cosmetic_solutions');?>">Cosmetic Solutions</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/cart');?>">Cart</a>
										</li>
										<li>
											<a href="<?= base_url('index.php/Welcome/shop');?>">SHOP</a>
										</li>
									</ul>
								</div><!--/.col-xs-6 col-sm-6-->
							</div><!-- Medlife End -->
						</div><!--/.-->
						
						<div class="col-sm-6 col-md-3 widget-block">
							<h6>NEWSLETTER</h6><!-- Medlife Start -->
							<div class="news-letter-block">
								<p>Recieve our latest news straight to your inbox</p>
								<form action="" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
									<input class="email form-control" name="EMAIL" placeholder="Email address" required="" type="email" value="">
									<div aria-hidden="true" style="position: absolute; left: -5000px;">
										<input name="b_78185f3823fef6dc6a261e0df_2ebd195299" tabindex="-1" type="text" value="">
									</div><button class="btn btn-default" id="mc-embedded-subscribe2" name="SEND" type="submit">Subscribe</button>
									<div class="md__newsletter-message"></div>
								</form>
							</div><!-- Medlife End -->
						</div><!--/.col-sm-6 col-md-3-->
					</div><!--/.row-->
				</div><!-- F-Widget End -->
			</div><!-- Footer Top End -->
			<!-- Footer Bottom Start -->
			<div class="f-bottom clearfix">
				<p>Developed By Bikash Prajapati@2018. All rights reserved.</p>
				<div class="f-social-icons clearfix">
					<ul>
						<li>
							<a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a>
						</li>
						<li>
							<a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a>
						</li>
						<li>
							<a href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div><!--/.f-social-icons-->
			</div><!-- Footer Bottom End -->
		</div><!--/.container-->
	</footer><!-- Footer End -->
	<!-- Login Modal Start -->
	<div aria-hidden="true" class="modal fade login-modal" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Close Start -->
				<button class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <!-- Close End -->
				 <!-- Login Section Start -->
				<div class="login-block clearfix">
					<h6>SIGN IN YOUR ACCOUNT TO HAVE ACCESS TO DIFFERENT FEATURES</h6>
					
						<?php echo form_open('Login_controller', ['role'=>'form', 'method'=>'POST']);?>
						<div class="form-group">
							<label for="exampleInputtext1">Username</label> 
							<?php echo form_input(['class'=>'form-control','id'=>'exampleInputtext1','placeholder'=>'eg: Bikash', 'type'=>'text','name'=>'Username']);?>
							
						</div><!--/.form-group-->
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label> 
							<?php echo form_input(['class'=>'form-control','id'=>'exampleInputPassword1','placeholder'=>'password','type'=>'password','name'=>'password']);?>

						</div><!--/.form-group-->
						<div class="checkbox">
							<label><input type="checkbox"> REMEMBER ME</label>
						</div><!--/.checkbox-->
						<button class="btn btn-default" type="submit">Log in</button>
						<div class="forgot-password">
							<a href="#">FORGOT YOUR PASSWORD?</a>
						</div><!--/.forgot-password-->
					</form>
				</div><!-- Login Section End -->
				<!-- Forgot Password Section Start -->
				<div class="forgot-password-block clearfix">
					<h6>FORGOT YOUR DETAILS?</h6>
					<form>
						<div class="form-group">
							<label for="exampleInputtext2">Username OR Email</label> <input class="form-control" id="exampleInputtext2" placeholder="eg: james_smith" type="text">
						</div><button class="btn btn-default" type="submit">SEND MY DETAILS!</button>
						<div class="forgot-password">
							<a href="#">AAH, WAIT, I REMEMBER NOW!</a>
						</div>
					</form>
				</div><!-- Forgot Password Section End -->
			</div><!--/.modal-content-->
		</div><!--/.modal-dialog-->
	</div><!-- Login Modal End -->