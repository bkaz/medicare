<!DOCTYPE html>
<html lang="eng">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>Hospital</title><!-- Favicon -->
	<?php
	 // echo base_url();
	?>
	<link href="images/favicon.ico" rel="icon" type="image/x-icon"><!-- Font Awesome -->
	
	<?= link_tag('assets/css/font-awesome.min.css');?>
	<!-- Bootstrap -->
	
	<?= link_tag('assets/css/bootstrap.css');?>
	<?= link_tag('assets/css/style.css');?>
	<?= link_tag('assets/fonts/fonts.css');?>
	<!-- Yamm Megamenu -->
	<?= link_tag('assets/css/yamm.css');?>
	<!-- Animation -->
	<?= link_tag('assets/css/animate.css');?>
			<!-- Jquery Ui Date Picker -->
	<!-- Range Slider Start -->
	<?= link_tag('assets/css/jquery-ui.css');?>
	<?= link_tag('assets/css/jquery-ui-slider-pips.css');?>
	<!-- Range Slider End -->
	<!-- Jquery Ui Date Picker -->
	<!-- Boostrap Time Picker -->
	<?= link_tag('assets/css/jquery.timepicker.css');?>
	<!-- Selectric Start -->
	<?= link_tag('assets/css/selectric.css');?>
	<!-- Time Table Start -->
	<?= link_tag('assets/css/reset.css');?>
	<?= link_tag('assets/css/time-table.css');?>
	<!-- Time Table End -->

	<!-- Flat Icon -->
	<?= link_tag('assets/fonts/flaticon.css');?>
	<!-- Flat Icon -->
	<!-- Multi Level Push Menu -->
	<?= link_tag('assets/css/normalize.css');?>
	<?= link_tag('assets/css/component.css');?>
	<!-- REVOLUTION STYLE SHEETS -->
	<?= link_tag('assets/js/revslider/settings.css');?>
	<?= link_tag('assets/js/revslider/layers.css');?>
	<?= link_tag('assets/js/revslider/navigation.css');?>
	<!-- REVOLUTION LAYERS STYLES -->
	<script src="<?php echo base_url('assets/js/modernizr.custom.js');?>">
	</script><!-- Multi Level Push Menu -->
	
</head>
<body>
	<!-- Header Start -->
	<header class="header clearfix">
		<!-- Header Top Start -->
		<div class="h-top clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-md-4">
						<div class="h-left">
							<p><span><i aria-hidden="true" class="fa fa-clock-o"></i> OPENED ON WEEKDAYS FROM</span> 8:30 AM TO 6:30 PM</p>
						</div>
					</div>
					<div class="col-sm-8 col-md-8">
						<div class="h-right">
							<!-- History Policy Start -->
							<div class="h-history-policy clearfix">
								
								<ul>
									<li>
										<a class="hidden-xs" href="<?= base_url('index.php/Welcome/signup');?>">Register</a>
									</li>
									<li>
										<a class="hidden-xs" data-target=".login-modal" data-toggle="modal" href="#"> User Login</a> <a class="visible-xs" data-target=".login-modal" data-toggle="modal" href="#"></a>
									</li>
									<li>
										<a class="hidden-xs" href="<?= base_url('index.php/Admin_Controller');?>">Admin Login</a>
									</li>
									
								</ul>
							
							</div><!-- History Policy End -->
							
							
						</div><!--/.h-right-->
					</div><!--/.col-sm-8 col-md-8"-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!-- Header Top End -->
		<!-- Header Bottom Start -->
		<div class="h-bottom clearfix">
			<div class="container">
				<div class="navigation-menu">
					<div class="navbar yamm navbar-default">
						<div class="navbar-header">
							<a class="navbar-brand" href="<?= base_url('index.php/Welcome');?>">
								<img alt="" class="hidden-xs hidden-sm" src="<?php echo base_url('assets/images/logo.png')?>"> 
								<img alt="" class="visible-xs visible-sm" src="<?php echo base_url('assets/images/mobile-logo.png')?>"></a>
						</div><!-- Desktop Navigation List Start -->
						<div class="navbar-collapse collapse hidden-xs" id="navbar-collapse-1">
							<div class="background"></div>
							<ul class="nav navbar-nav">
								
								<li class="dropdown yamm-fw">
									<a class="dropdown-toggle" href="<?php echo base_url('index.php/Welcome/service');?>">SERVICES</a>
									<ul class="dropdown-menu">
										<li>
											<!-- Content container to add padding -->
											<div class="yamm-content">
												<div class="row">
													<ul class="col-md-4 list-unstyled">
														<li>
															<a class="mega-menu-title" href="#">CATEGORY 1</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/general_service')?>">General & Preventive Care</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/cosmetic_solutions')?>">Cosmetic Solutions</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/restorative_service')?>">Restorative Solutions</a>
														</li>
													</ul><!--/.col-md-3-->
													<ul class="col-md-4 list-unstyled">
														<li>
															<a class="mega-menu-title" href="#">CATEGORY 2</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/additional_service')?>">Additional Treatments</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/orthodontics_service')?>">Orthodontics</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/dentures')?>">Dentures & Denture Repair</a>
														</li>
													</ul><!--/.col-md-3-->
													<ul class="col-md-4 list-unstyled">
														<li>
															<a class="mega-menu-title" href="#">CATEGORY 3</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/diagonsis')?>">Diagnostic & Preventive</a>
														</li>
														<li>
															<a href="<?= base_url('index.php/Welcome/pediatric_service')?>">Pediatric Dentistry</a>
														</li>
														
													</ul><!--/.col-md-3-->
													
												</div><!--/.row-->
											</div><!--/.yamm-content-->
										</li>
									</ul><!--/.dropdown-menu-->
								</li><!--/.dropdown yamm-fw active-->
								<li>
									<a href="<?= base_url('index.php/Welcome/doctor')?>">DOCTORS</a>
								</li>
								<li>
									<a href="<?= base_url('index.php/Welcome/timetabe')?>">TIMETABLE</a>
								</li>
							
								
								<li>
									<a href="<?= base_url('index.php/Welcome/shop')?>">SHOP</a>
								</li>
								<li>
									<a href="<?= base_url('index.php/Welcome/contact')?>">CONTACT</a>
								</li>
							</ul><!--/.nav navbar-nav-->
						</div><!-- Desktop Navigation List End -->
						<!-- Mobile Navigation List Start -->
						<div class="dl-menuwrapper visible-xs" id="dl-menu">
							<button class="dl-trigger">Open Menu</button>
							<div class="background"></div>
							<ul class="dl-menu">
								
								<li class="active">
									<a href="service.php">SERVICES</a>
									<ul class="dl-submenu">
										<li>
											<a href="#">Category 1</a>
											<ul class="dl-submenu">
												<li>
													<a href="<?= base_url('index.php/Welcome/general_service')?>">General & Preventive Care</a>
												</li>
												<li>
													<a href="<?= base_url('index.php/Welcome/cosmetic_solutions')?>">Cosmetic Solutions</a>
												</li>
												<li>
													<a href="<?= base_url('index.php/Welcome/restorative_service')?>">Restorative Solutions</a>
												</li>
											</ul><!--/.dl-submenu-->
										</li>
										<li>
											<a href="#">Category 2</a>
											<ul class="dl-submenu">
												<li>
													<a href="<?= base_url('index.php/Welcome/additional_service')?>">Additional Treatments</a>
												</li>
												<li>
													<a href="<?= base_url('index.php/Welcome/orthodontics_service')?>">Orthodontics</a>
												</li>
												<li>
													<a href="<?= base_url('index.php/Welcome/dentures')?>">Dentures & Denture Repair</a>
												</li>
											</ul><!--/.dl-submenu-->
										</li>
										<li>
											<a href="#">Category 3</a>
											<ul class="dl-submenu">
												<li class="active">
													<a href="<?= base_url('index.php/Welcome/diagonsis')?>">Diagnostic & Preventive</a>
												</li>
												<li>
													<a href="<?= base_url('index.php/Welcome/pediatric_service')?>">Pediatric Dentistry</a>
												</li>
												
											</ul><!--/.dl-submenu-->
										</li>
									
									</ul><!--/.dl-submenu-->
								</li>
								<li>
									<a href="<?= base_url('index.php/Welcome/doctor')?>">DOCTORS</a>
								</li>
								<li>
									<a href="<?= base_url('index.php/Welcome/timetable')?>">TIMETABLE</a>
								</li>
							
								
								<li>
									<a href="<?= base_url('index.php/Welcome/shop')?>">SHOP</a>
								</li>
								<li>
									<a href="<?= base_url('index.php/Welcome/contact')?>">CONTACT</a>
								</li>
							</ul><!--/.dl-menu-->
						</div><!-- /dl-menuwrapper -->
						<!-- Mobile Navigation List End -->
						<!-- Cart Right Start -->
						<div class="cart-right">
							<a class="phone-btn" href="#"><i aria-hidden="true" class="fa fa-phone"></i>+44 725-555-1234</a>
							<div class="header-cart">
								<a class="headercartbtn" href="#"><i aria-hidden="true" class="fa fa-shopping-cart"></i> <span>1</span></a>
								<div class="cart-down-panel">
									<ul class="cart-list">
										<li class="mini-cart-item">
											<button aria-label="Close" class="close" type="button"><span aria-hidden="true">&times;</span></button>
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4">
													<div class="mini-cart-image"><img alt="" class="img-responsive" src="<?php echo base_url('assets/images/mini-cart-img.jpg')?>"></div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 pad-left">
													<div class="mini-cart-detail">
														<a href="#">Oven Roasted duck with special sousage</a>
													</div>
												</div><!--/.col-xs-8 col-sm-8 col-md-8 pad-left-->
											</div><!--/.row-->
										</li><!--/.mini-cart-item-->
									</ul><!--/.cart-list-->
									<div class="total">
										<div class="row">
											<div class="col-sm-6 col-md-6">
												<div class="total-left">
													<p>subtotal</p>
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="total-right">
													<p>$35</p>
												</div>
											</div><!--/.col-sm-6 col-md-6-->
										</div><!--/.row"-->
									</div><!--/.total-->
									<div class="mini-cart-btn">
										<a class="btn btn-default view-cart" href="<?= base_url('index.php/Welcome/cart');?>">view cart</a> <a class="btn btn-simple checkout" href="#">checkout</a>
									</div><!--/.mini-cart-btn-->
								</div><!--/.cart-down-panel-->
							</div><!--/.header-cart-->
						</div><!-- Cart Right End -->
					</div><!--/.navbar yamm navbar-default-->
				</div><!--/.navigation-menu-->
			</div><!--/.container-->
		</div><!-- Header Bottom End -->
	</header><!-- Header End -->


	<script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js');?>">
	</script> <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	 <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->
	 
		<script data-cfasync="false" src="<?php echo base_url('assets/js/email-decode.min.js')?>"></script>
	 <!-- REVOLUTION JS FILES -->
	<script src="<?php echo base_url('assets/js/revslider/jquery.themepunch.tools.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/jquery.themepunch.revolution.min.js')?>">
	</script> <!-- REVOLUTION JS FILES -->
	 <!-- SLIDER REVOLUTION -->
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.actions.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.layeranimation.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.navigation.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.parallax.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.video.min.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/revslider/revolution.extension.slideanims.min.js')?>">
	</script> <!-- SLIDER REVOLUTION -->
	 <!-- Parallax Start -->
	<script src="<?php echo base_url('assets/js/parallax.js')?>">
	</script> <!-- Parallax End -->
	 <!-- Ofi-Script -->
	<script src="<?php echo base_url('assets/js/ofi.js')?>">
	</script> <!-- Ofi-Script -->
	 <!-- Mobile Menu Js Start -->
	<script src="<?php echo base_url('assets/js/jquery.dlmenu.js')?>">
	</script> <!-- Mobile Menu Js End -->
	<script src="<?php echo base_url('assets/js/slick.js')?>">
	</script> <!-- Slick Sider -->
	<!-- Selectric Start -->
	<script src="<?php echo base_url('assets/js/jquery.selectric.js')?>">
	</script> <!-- Selectric End -->
	<script src="<?php echo base_url('assets/js/jquery-ui.js')?>">
	</script> <!-- Range Slide -->
	<!-- Jquery Ui Date Picker -->
	<!-- Bootstrap Time Picker -->
	<script src="<?php echo base_url('assets/js/jquery.timepicker.js')?>">
	</script> <!-- Bootstrap Time Picker -->
	 <!-- Time Table Start -->
	<script src="<?php echo base_url('assets/js/modernizr.js')?>">
	</script>
	<script src="<?php echo base_url('assets/js/main.js')?>">
	</script> <!-- Time Table End -->
	<script src="<?php echo base_url('assets/js/jquery.ui.touch-punch.min.js')?>">
	</script> <!-- Extra Js -->

	<script>
		

		"use strict";
		//Rev slider
		var setREVStartSize=function(){
			 try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
				e.c = jQuery('#rev_slider_1_1');
				e.responsiveLevels = [1240,1024,778,480];
				e.gridwidth = [1240,992,778,480];
				e.gridheight = [868,768,960,720];
				e.sliderLayout = "fullwidth";
				if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
			}catch(d){console.log("Failure at Presize of Slider:"+d)}
		};
		setREVStartSize();

		var tpj=jQuery;
		var revapi1;
		tpj(document).ready(function() {
			if(tpj("#rev_slider_1_1").revolution == undefined){
				revslider_showDoubleJqueryError("#rev_slider_1_1");
			}else{
				revapi1 = tpj("#rev_slider_1_1").show().revolution({
				sliderType:"standard",
				jsFileLocation:"js/",
				sliderLayout:"auto",
				dottedOverlay:"none",
				delay:9000,
				navigation: {
					 keyboardNavigation:"off",
					 keyboard_direction: "horizontal",
					 mouseScrollNavigation:"off",
					mouseScrollReverse:"default",
					 onHoverStop:"off",
					 touch:{
						touchenabled:"on",
						swipe_threshold: 75,
						swipe_min_touches: 1,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					 },
					arrows: {
						style:"new-bullet-bar",
						enable:true,
						hide_onmobile:true,
						hide_under:778,
						hide_onleave:false,
						left: {
							 h_align:"left",
							v_align:"center",
							 h_offset:30,
							 v_offset:0
						},
						right: {
							 h_align:"right",
							 v_align:"center",
							h_offset:30,
							 v_offset:0
						}
					},
					 bullets: {
						enable:true,
						hide_onmobile:true,
						hide_under:992,
						style:"uranus",
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1200,
						direction:"horizontal",
						h_align:"center",
						v_align:"bottom",
						h_offset:0,
						v_offset:220,
						space:10,
						tmp:'<span class="tp-bullet-inner"><\/span>'
					}
				},
				responsiveLevels:[1240,1024,778,480],
				visibilityLevels:[1240,1024,778,480],
				gridwidth:[1240,1024,778,480],
				gridheight:[900,768,960,720],
				lazyType:"none",
				parallax: {
					 type:"scroll",
					 origo:"slidercenter",
					 speed:1000,
					 speedbg:0,
					 speedls:1000,
					 levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,30],
				},
				shadow:0,
				spinner:"spinner5",
				stopLoop:"off",
				stopAfterLoops:-1,
				stopAtSlide:-1,
				shuffle:"off",
				autoHeight:"off",
				disableProgressBar:"on",
				fullScreenAutoWidth:"off",
				fullScreenAlignForce:"off",
				fullScreenOffsetContainer: "",
				fullScreenOffset: "",
				hideThumbsOnMobile:"off",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
			});
			jQuery(document).ready(function() {
				jQuery('.hover1').on('hover', function() {
					jQuery('.arrow1').stop().animate({
						'margin-left': '10px'
					}, 200);
				}, function() {
					jQuery('.arrow1').stop().animate({
						'margin-left': '0px'
					}, 200);
				});
				jQuery('.hover2').on('hover', function() {
					jQuery('.arrow2').stop().animate({
						'margin-left': '10px'
					}, 200);
				}, function() {
					jQuery('.arrow2').stop().animate({
						'margin-left': '0px'
					}, 200);
				});
				jQuery('.hover3').on('hover', function() {
					jQuery('.arrow3').stop().animate({
						'margin-left': '10px'
					}, 200);
				}, function() {
					jQuery('.arrow3').stop().animate({
						'margin-left': '0px'
					}, 200);
				});
			});
			 }
		}); /*ready*/

	</script>
	<script src="<?php echo base_url('assets/js/script.js')?>">
	</script>