<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php include "includes/header.php";?>
	<!-- Content Start -->
	<section class="content contact-pg clearfix">
		<!-- Banner Start -->
		<div class="banner inner-banner clearfix">
			<img alt="BANNER" class="img-responsive" src="<?= base_url('assets/images/slider-img10.jpg');?>">
			<div class="banner-overlay"></div><!-- Banner Detail Start -->
			<div class="banner-desc clearfix">
				<div class="container">
					<h1>Providing Total Health Care Solution</h1>
				</div>
			</div><!-- Banner Detail End -->
		</div><!-- Banner End -->
		<!-- Inner Pages Start -->
		<div class="inner-content clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-9">
						<div class="content-desc">
							<!-- Appointment Form Start -->
							<?php include "appointment_form.php";?>	
							<!-- Appointment Form End -->
						</div><!--/.content-desc-->
					</div><!--/.col-sm-12 col-md-8 col-lg-9-->
					<div class="col-sm-12 col-md-4 col-lg-3">
						<!-- Sidebar Widget Start -->
						<div class="sidebar-widget clearfix">
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- Opening Hover Start -->
								<div class="opening-hours light-green-bg">
									<!-- Widget Title Start -->
									<div class="widget-title clearfix">
										<h6>Opening Hours</h6>
									</div><!-- Widget Title End -->
									<table class="table">
										<tbody>
											<tr>
												<td>Monday - Friday:</td>
												<td>8.30 - 18.30</td>
											</tr>
											<tr>
												<td>Saturday:</td>
												<td>10.30 - 16.30</td>
											</tr>
											<tr>
												<td>Sunday:</td>
												<td>10.30 - 16:30</td>
											</tr>
										</tbody>
									</table>
								</div><!-- Opening Hover End -->
							</div><!-- Widget Block End -->
							<!-- Widget Block Start -->
							<div class="widget-block">
								<!-- Map Block Start -->
								<div class="map clearfix">
									<iframe allowfullscreen src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.509895529668!2d-72.8474816841276!3d41.44985497925848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e7ce55dd9d1353%3A0xeebd80135a356c3e!2sOakdale+Theatre!5e0!3m2!1sen!2sin!4v1507367244654" style="width: 100%; height: 262px; border:0;"></iframe>
								</div><!-- Map Block End -->
							</div><!-- Widget Block End -->
						</div><!-- Sidebar Widget End -->
					</div><!--/.col-sm-12 col-md-4 col-lg-3-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!-- Inner Pages End -->
	</section><!-- Content End -->
	<?php include "includes/footer.php";?>
	
	<!-- Back To Top Start -->
	<div class="back-to-top clearfix">
		<a href="#"><span><i aria-hidden="true" class="fa fa-chevron-up"></i> Top</span></a>
	</div><!-- Back To Top End -->
	