<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php include "includes/header.php";?>
<?= link_tag('assets/css/admin_login.css');?>
<script src="<?= base_url('assets/js/admin_login_form.js');?>"></script>
<div class="container">
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <section class="login-form">
     
          <?php echo form_open('Admin_controller/admin_login', ['method'=>'post','action'=>'','role'=>'login'])?>
          <h1> Admin Login </h1>

          <?php echo form_input(['type'=>'email','name'=>'email','placeholder'=>'Email','required'=>'','class'=>'form-control input-lg','value'=>'admin@admin.com'])?>
          
          <?php echo form_input(['type'=>'password','class'=>'form-control input-lg','id'=>'password','placeholder'=>'Password','required'=>'','value'=>'admin@123'])?>
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Sign in</button>
          <div>
          </div>
        </form>
      </section>  
    </div>
  </div> 
</div>

<?php include "includes/footer.php";?>