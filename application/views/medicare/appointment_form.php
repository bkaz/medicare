<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Appointment Form Start -->
							<div class="appointment-form white-bg clearfix">
								<h6>Make an Appointment</h6>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<div class="row">
									<?php echo form_open('User_appointment/add_appointment',['class'=>'md__contactForm', 'method'=>'post','action'=>''])?>
										<div class="col-sm-6 col-md-6 select-box">
											<div class="form-group">
												<label for="exampleInputtext1">Select Clinic</label> <select class="form-control md__input" name="clinic_name">
													<option>
														Medlife Annapolis
													</option>
													<option>
														Medlife Germantown
													</option>
													<option>
														Medlife Columbia
													</option>
													<option>
														Medlife Olney
													</option>
													<option>
														Medlife Pasadena
													</option>
													<option>
														Medlife Pikesville
													</option>
													<option>
														Medlife Waldorf
													</option>
												</select>
											</div><!--/.form-group-->
										</div>
										<div class="col-sm-6 col-md-6 select-box">
											<div class="form-group">
												<label>Service</label> <select class="form-control md__input" name="service_name">
													<option>
														General &amp; Preventive Care
													</option>
													<option>
														Cosmetic Solutions
													</option>
													<option>
														Restorative Solutions
													</option>
													<option>
														Additional Treatments
													</option>
													<option>
														Orthodontics
													</option>
													<option>
														Dentures &amp; Denture Repair
													</option>
													<option>
														Diagnostic &amp; Preventive
													</option>
													<option>
														Diagnostic &amp; Preventive
													</option>
													<option>
														Pediatric Dentistry
													</option>
												</select>
											</div><!--/.form-group-->
										</div><!--/.col-sm-6 col-md-6-->
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label>Name</label> 
												<input class=" md__input form-control" id="exampleInputtext3" name="patient_name" type="text" required="">
											</div><!--/.form-group-->
										</div><!--/.col-sm-6 col-md-6-->
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label for="exampleInputtext4">Phone</label> 
												<input class=" md__input form-control" id="exampleInputtext4" name="phone_number" required="" type="text">
											</div><!--/.form-group-->
										</div><!--/.col-sm-6 col-md-6-->
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label for="exampleInputEmail1">Email</label> <input class=" md__input form-control" id="exampleInputEmail1" name="email" type="email" required="">
											</div><!--/.form-group-->
										</div><!--/.col-sm-6 col-md-6-->
										<div class="col-sm-6 col-md-6">
											<div class="form-group date">
												<label class="date">Date</label> <input class=" md__input form-control" id="datepicker" name="date" type="text" required="">
											</div><!--/.form-group-->
											<div class="form-group time">
												<label class="time">Pick Time</label> <input class=" md__input form-control" id="time" name="time" type="text" required="">
											</div><!--/.form-group-->
										</div><!--/.col-sm-6 col-md-6-->
										<!--/.col-sm-12-->
										<button class="btn btn-default appointment-button md__submitBtn" type="submit">Make an Appointment</button>
									</form><!--/.md__contactForm-->
								</div><!--/.row-->
							</div><!-- Appointment Form End -->