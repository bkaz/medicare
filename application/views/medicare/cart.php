<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php include 'includes/header.php';?>

	<!-- Content Start -->
	<section class="content inner-pg shop-pg shop-cart-pg clearfix">
		<!-- Breadcrumb Start -->
		<div class="breadcrumb-title clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-left">
							<ol class="breadcrumb">
								<li>
									<a href="<?= base_url('index.php/Welcome');?>">HOME</a>
								</li>
								<li class="active">Cart</li>
							</ol>
						</div>
					</div>
					<div class="col-sm-6 col-md-6">
						<div class="breadcrumb-right">
							<h5>Cart</h5>
						</div>
					</div><!--/.col-sm-6 col-md-6-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!-- Breadcrumb End -->
		<div class="container">
			<!-- Inner Pages Start -->
			<div class="inner-content clearfix">
				<!-- Content Description Start -->
				<div class="content-desc clearfix">
					<!-- Section Title Start -->
					<div class="section-title">
						<h1>Cart</h1>
					</div><!-- Section Title End -->
					<!-- Cart Error Message Start -->
					<div class="cart-error-msg clearfix">
						<p>Your cart is currently empty.</p>
					</div><!-- Cart Error Message End -->
					<!-- Return To Shop Button Start -->
					<div class="return-shop-btn clearfix">
						<a class="btn btn-default" href="#" role="button">Return To Shop</a>
					</div><!-- Return To Shop Button End -->
					<form class="woocommerce-cart-form">
						<div class="table-responsive">
							<table class="table cart-table table-hover">
								<thead>
									<tr>
										<th class="product-remove">&nbsp;</th>
										<th class="product-thumbnail">&nbsp;</th>
										<th class="product-name">Product</th>
										<th class="product-price">Price</th>
										<th class="product-quantity">Quantity</th>
										<th class="product-subtotal">Total</th>
									</tr>
								</thead>
								<tbody>
									<tr class="cart-item">
										<td class="product-remove">
											<a class="remove" href="#"></a>
										</td>
										<td class="product-thumbnail">
											<a href="#"><img alt="" class="img-responsive" src="<?= base_url('assets/images/shop-product5.jpg');?>"></a>
										</td>
										<td class="product-name" data-title="Product">
											<a href="#">DiamondClean Rechargeable Electric Toothbrush</a>
										</td>
										<td class="product-price" data-title="Price"><span>$149.00</span></td>
										<td class="product-quantity" data-title="Quantity">
											<div class="quantity">
												<input class="input-text qty text" type="number" value="1">
											</div>
										</td>
										<td class="product-subtotal" data-title="Total"><span>$149.00</span></td>
									</tr><!--/.--><!--/.cart-item-->
									<tr class="cart-item">
										<td class="product-remove">
											<a class="remove" href="#"></a>
										</td>
										<td class="product-thumbnail">
											<a href="#"><img alt="" class="img-responsive" src="<?= base_url('assets/images/shop-product5.jpg');?>"></a>
										</td>
										<td class="product-name" data-title="Product">
											<a href="#">DiamondClean Rechargeable Electric Toothbrush</a>
										</td>
										<td class="product-price" data-title="Price"><span>$149.00</span></td>
										<td class="product-quantity" data-title="Quantity">
											<div class="quantity">
												<input class="input-text qty text" type="number" value="1">
											</div>
										</td>
										<td class="product-subtotal" data-title="Total"><span>$149.00</span></td>
									</tr><!--/.cart-item-->
									<tr>
										<td class="actions" colspan="6">
											<div class="coupon">
												<input class="input-text" id="coupon_code" name="coupon_code" placeholder="Coupon code" type="text" value=""> <input class="button" name="apply_coupon" type="submit" value="Apply coupon">
											</div>
											<div class="update-cart">
												<a class="btn btn-default" href="#">Update Cart</a>
											</div>
										</td>
									</tr><!--/.cart-item-->
								</tbody>
							</table>
						</div><!--/.table-responsive-->
					</form>
					<div class="cart-collaterals">
						<div class="row">
							<div class="col-sm-push-4 col-sm-8 col-md-push-6 col-md-6 col-lg-4 col-lg-push-8">
								<div class="cart-totals calculated-shipping">
									<h2>Cart totals</h2>
									<table class="shop-table shop-table-responsive">
										<tbody>
											<tr class="cart-subtotal">
												<th>Subtotal</th>
												<td data-title="Subtotal"><span class="woocommerce-Price-amount amount">$298.00</span></td>
											</tr><!--/.cart-subtotal-->
											<tr class="order-total">
												<th>Total</th>
												<td data-title="Total"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>298.00</span></strong></td>
											</tr><!--/.order-total-->
										</tbody>
									</table><!--/.shop-table-->
									<div class="proceed-to-checkout">
										<a class="btn btn-default" href="#">Proceed to checkout</a>
									</div>
								</div><!--/.cart-totals-->
							</div><!--/.col-sm-push-4 col-sm-8-->
						</div><!--/.row-->
					</div><!--/.cart-collaterals-->
				</div><!-- Content Description End -->
			</div><!-- Inner Pages End -->
		</div><!--/.container-->
	</section><!-- Content End -->
	<!-- Footer Start -->
	<?php include "includes/footer.php";?>

	<!-- Back To Top Start -->
	<div class="back-to-top clearfix">
		<a href="#"><span><i aria-hidden="true" class="fa fa-chevron-up"></i> Top</span></a>
	</div><!-- Back To Top End -->
