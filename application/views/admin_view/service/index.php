<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Service Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('service/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Service Id</th>
						<th>Service Name</th>
						<th>Service Type</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($service as $s){ ?>
                    <tr>
						<td><?php echo $s['service_id']; ?></td>
						<td><?php echo $s['service_name']; ?></td>
						<td><?php echo $s['service_type']; ?></td>
						<td><?php echo $s['description']; ?></td>
						<td>
                            <a href="<?php echo site_url('service/edit/'.$s['service_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('service/remove/'.$s['service_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
