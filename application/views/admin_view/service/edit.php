<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Service Edit</h3>
            </div>
			<?php echo form_open('service/edit/'.$service['service_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="service_name" class="control-label">Service Name</label>
						<div class="form-group">
							<input type="text" name="service_name" value="<?php echo ($this->input->post('service_name') ? $this->input->post('service_name') : $service['service_name']); ?>" class="form-control" id="service_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="service_type" class="control-label">Service Type</label>
						<div class="form-group">
							<input type="text" name="service_type" value="<?php echo ($this->input->post('service_type') ? $this->input->post('service_type') : $service['service_type']); ?>" class="form-control" id="service_type" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="description" class="control-label">Description</label>
						<div class="form-group">
							<input type="text" name="description" value="<?php echo ($this->input->post('description') ? $this->input->post('description') : $service['description']); ?>" class="form-control" id="description" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>