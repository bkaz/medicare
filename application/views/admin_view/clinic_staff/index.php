<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Clinic Staff Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('admin_panel/clinic_staff/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Staff Id</th>
						<th>Staff Name</th>
						<th>Staff Post</th>
						<th>Staff Description</th>
						<th>Staff Status</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($clinic_staff as $c){ ?>
                    <tr>
						<td><?php echo $c['staff_id']; ?></td>
						<td><?php echo $c['staff_name']; ?></td>
						<td><?php echo $c['staff_post']; ?></td>
						<td><?php echo $c['staff_description']; ?></td>
						<td><?php echo $c['staff_status']; ?></td>
						<td>
                            <a href="<?php echo site_url('admin_panel/clinic_staff/edit/'.$c['staff_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('admin_panel/clinic_staff/remove/'.$c['staff_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
