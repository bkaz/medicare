<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Clinic Staff Add</h3>
            </div>
            <?php echo form_open('admin_panel/clinic_staff/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="staff_name" class="control-label">Staff Name</label>
						<div class="form-group">
							<input type="text" name="staff_name" value="<?php echo $this->input->post('staff_name'); ?>" class="form-control" id="staff_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="staff_post" class="control-label">Staff Post</label>
						<div class="form-group">
							<input type="text" name="staff_post" value="<?php echo $this->input->post('staff_post'); ?>" class="form-control" id="staff_post" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="staff_description" class="control-label">Staff Description</label>
						<div class="form-group">
							<input type="text" name="staff_description" value="<?php echo $this->input->post('staff_description'); ?>" class="form-control" id="staff_description" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="staff_status" class="control-label">Staff Status</label>
						<div class="form-group">
							<input type="text" name="staff_status" value="<?php echo $this->input->post('staff_status'); ?>" class="form-control" id="staff_status" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>