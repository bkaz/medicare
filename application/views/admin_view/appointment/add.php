<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Appointment Add</h3>
            </div>
            <?php echo form_open('admin_panel/appointment/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="clinic_name" class="control-label">Clinic Name</label>
						<div class="form-group">
							<input type="text" name="clinic_name" value="<?php echo $this->input->post('clinic_name'); ?>" class="form-control" id="clinic_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="service_name" class="control-label">Service Name</label>
						<div class="form-group">
							<input type="text" name="service_name" value="<?php echo $this->input->post('service_name'); ?>" class="form-control" id="service_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="patient_name" class="control-label">Patient Name</label>
						<div class="form-group">
							<input type="text" name="patient_name" value="<?php echo $this->input->post('patient_name'); ?>" class="form-control" id="patient_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="phone_number" class="control-label">Phone Number</label>
						<div class="form-group">
							<input type="text" name="phone_number" value="<?php echo $this->input->post('phone_number'); ?>" class="form-control" id="phone_number" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date" class="control-label">Date</label>
						<div class="form-group">
							<input type="text" name="date" value="<?php echo $this->input->post('date'); ?>" class="has-datepicker form-control" id="date" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="time" class="control-label">Time</label>
						<div class="form-group">
							<input type="text" name="time" value="<?php echo $this->input->post('time'); ?>" class="form-control" id="time" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="doctor_id" class="control-label">Doctor Id</label>
						<div class="form-group">
							<input type="text" name="doctor_id" value="<?php echo $this->input->post('doctor_id'); ?>" class="form-control" id="doctor_id" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>