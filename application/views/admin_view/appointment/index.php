<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Appointment Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('admin_panel/appointment/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Appointment Id</th>
						<th>Clinic Name</th>
						<th>Service Name</th>
						<th>Patient Name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Date</th>
						<th>Time</th>
						<th>Doctor Id</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($appointment as $a){ ?>
                    <tr>
						<td><?php echo $a['appointment_id']; ?></td>
						<td><?php echo $a['clinic_name']; ?></td>
						<td><?php echo $a['service_name']; ?></td>
						<td><?php echo $a['patient_name']; ?></td>
						<td><?php echo $a['phone_number']; ?></td>
						<td><?php echo $a['email']; ?></td>
						<td><?php echo $a['date']; ?></td>
						<td><?php echo $a['time']; ?></td>
						<td><?php echo $a['doctor_id']; ?></td>
						<td>
                            <a href="<?php echo site_url('admin_panel/appointment/edit/'.$a['appointment_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('admin_panel/appointment/remove/'.$a['appointment_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
