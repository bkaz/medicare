<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Cliniic Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('admin_panel/cliniic/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Clinic Id</th>
						<th>Clinic Name</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($cliniic as $c){ ?>
                    <tr>
						<td><?php echo $c['clinic_id']; ?></td>
						<td><?php echo $c['clinic_name']; ?></td>
						<td>
                            <a href="<?php echo site_url('admin_panel/cliniic/edit/'.$c['clinic_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('admin_panel/cliniic/remove/'.$c['clinic_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
